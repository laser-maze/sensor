import network

def connect(ssid, pwd, verbose=False):
    """Connects to Wifi with specified SSID and Password. 
    In case of dropped connection it will automatically reconnect. """
    sta_if = network.WLAN(network.STA_IF)
    if not sta_if.isconnected():
        if(verbose):
            print('connecting to network...')
        sta_if.active(True)
        sta_if.connect(ssid, pwd)
        while not sta_if.isconnected():
            pass
    if(verbose):
        print('network config:', sta_if.ifconfig())

def connected():
    sta_if = network.WLAN(network.STA_IF)
    return sta_if.isconnected()