from config import DEVICE_ID, WIFI_ESSID, WIFI_PASSWORD, MQTT_BROKER
from machine import Pin
import time

from umqtt.simple import MQTTClient #as MQTT # library is called umqtt.simple
import wifi_connect as wifi

Numbers = { "D0": 16, "D1": 5, "D2": 4, "D3": 0, "D4": 2, 
            "D5": 14, "D6": 12, "D7": 13, "D8": 15, 
            "RX": 3, "TX": 1}

VERBOSE = True
wifi.connect(WIFI_ESSID, WIFI_PASSWORD, VERBOSE)
#print("Wifi connected")

sensorIDs = ["D0", "D1", "D2", "D3", "D5", "D6", "D7", "D8"]    #D4 skipped - onboard LED. 
lastState = []  
sensors = []

for i in sensorIDs:
    sensors.append(Pin(Numbers[i], Pin.IN))
    lastState.append(sensors[-1].value())
#print("GPIO init done")

conn = MQTTClient(DEVICE_ID, MQTT_BROKER)
conn.connect()
#print("MQTT registered")

topic = "laser_maze/sensors/"+DEVICE_ID+"/"
devicesTopic = "laser_maze/devices/"    #where to announce new device - TODO automatic "sensor add" - with info about type and sensors. 
pingTopic = "laser_maze/ping/"+DEVICE_ID+"/"

printCounter=0
pingCounter=0

while True:
    if(wifi.connected() == True):
        for i in range(0, len(sensors)):
            currentValue = sensors[i].value()
            if(currentValue != lastState[i]):
                conn.publish(bytes(topic+str(i+1), "utf-8"), bytes(str(currentValue), "utf-8"))
                lastState[i] = currentValue
                if(VERBOSE):
                    print("Sensor", str(i)+":", currentValue)
    else:
        if(VERBOSE):
            print("Wifi NOT connected!")
    
    if(pingCounter ==40):
        conn.publish(bytes(pingTopic, "utf-8"), bytes(str(DEVICE_ID), "utf-8"))
        pingCounter=0
    else:
        pingCounter +=1
    
    if(VERBOSE):
        if(printCounter ==40):
            print("Loop")
            printCounter=0
        else:
            printCounter +=1
    time.sleep_ms(50)
conn.disconnect()